def standardize_names(character_name):
    standardize = '-'.join(character_name.split())
    return standardize 

def standardize_title(title):
    standardize = title.title()
    return standardize

def standardize_text(text):
    text = ' '.join(text.split())
    text_split = text.split('.')
    standardize = ''
    for i, sentence in enumerate(text_split):   
        if len(text_split) - 1 != i:
            aux = sentence.strip()
            sentence_capitalize = (aux[0].upper() + aux[1:]) + '. '
            standardize += sentence_capitalize
    return standardize.rstrip()

def title_creator(text):
    standardize = text.title().strip().center(40, '-')
    return standardize

def text_merge(text_of_blog_a, text_of_blog_b):
    text = ' '.join((text_of_blog_a + text_of_blog_b).split())
    text_split = text.split('.')
    standardize = ''
    for i, sentence in enumerate(text_split):
        if len(text_split) - 1 != i:
            aux = sentence.strip()
            sentence_capitalize = (aux[0].upper() + aux[1:]) + '. '
            standardize += sentence_capitalize
    return standardize.rstrip()
